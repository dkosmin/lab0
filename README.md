# README.md - CPS490 Lab 0

## Course Information

Examination of principles, practices, and methodology for the development of large software systems using data flow and object-oriented methodologies. User interface design, software testing, and software project management. Selecting and planning a team project; involves team formation, project selection, project planning, and proposal writing and presentation.

## My Information

![HeadShot](https://trello.com/1/cards/613192066421d71dca72d2cb/attachments/61325bd4ac906749528b1dff/download/1523767441039.jfif)

Name : Daniel Kosmin

Email : kosmind1@udayton.edu

## Repository Information

This Repository is to Complete the Requirements specified for Lab 0

Source: <https://bitbucket.org/dkosmin/lab0/src/master/>

## Team Repo Link

Source: <https://bitbucket.org/gabehoban/cps490f21-team5/src/master/>

## Team Trello Link

Source: <https://trello.com/b/OnhYeymu/scrum>
